import React, { useEffect, useState } from 'react';
import './css/App.css';
import MovieList from './components/MovieList';
import Navbar from './components/Navbar';
// import Sidebar from './components/Sidebar';
import axios from 'axios';

const Pagination = ({ moviesPerPage, totalMovies, paginate }) => {
	const pageNumbers = [];

	for (let i = 1; i < Math.ceil(totalMovies / moviesPerPage); i++) {
		pageNumbers.push(i);
	}

	return (
		<nav>
			<ul className="pagination">
				{pageNumbers.map((number) => (
					<li key={number} className="page-item">
						<a href="!#" className="page-link" onClick={() => paginate(number)}>
							{number}
						</a>
					</li>
				))}
			</ul>
		</nav>
	);
};

const App = () => {
	const [movies, setMovies] = useState([]);
	// const [genres, setGenres] = useState([]);
	const [state, setState] = useState({
		s: '',
		results: [],
		selected: {},
	});

	const [loading, setLoading] = useState(false);
	const [currentPage, setCurrentPage] = useState(1);
	const [moviesPerPage] = useState(14);

	useEffect(() => {
		const fetchMovies = async () => {
			setLoading(true);
			const res = await axios.get(
				'https://my-website-314506-service-r2hahu3hnq-lm.a.run.app/movies'
			);

			setMovies(res.data);
			setState((prevState) => {
				return {
					...prevState,
					results: res.data.sort(() => 0.5 - Math.random()),
				};
			});
			// setGenres(
			// 	res.data.reduce((acc, el) => {
			// 		if (acc.indexOf(el.genre) === -1) acc.push(el.genre);
			// 		return acc;
			// 	}, [])
			// );
			setLoading(false);
		};
		fetchMovies();
	}, []);

	const indexOfLastMovie = currentPage * moviesPerPage;
	const indexOfFirstMovie = indexOfLastMovie - moviesPerPage;

	const currentMovies =
		state.results.length < moviesPerPage
			? state.results
			: state.results.slice(indexOfFirstMovie, indexOfLastMovie);

	// const search = (e) => {
	// 	let results = movies.filter((el) =>
	// 		el.title.toLocaleLowerCase().includes(e.target.value.toLocaleLowerCase())
	// 	);

	// 	setState((prevState) => {
	// 		return { ...prevState, results: results };
	// 	});
	// };

	const handleInput = (e) => {
		let s = e.target.value;

		const res = movies.filter((el) =>
			el.title.toLocaleLowerCase().includes(s.toLocaleLowerCase())
		);

		if (state.results.movies < moviesPerPage) {
			paginate(1);
		}

		setState((prevState) => {
			return { ...prevState, results: res };
		});
	};

	const paginate = (pageNumber) => setCurrentPage(pageNumber);

	return (
		<div className="App">
			<Navbar movies={state.results} handleInput={handleInput} />
			<div className="Main">
				<div className="Movies">
					<MovieList movies={currentMovies} loading={loading} />
				</div>
			</div>
			<Pagination
				moviesPerPage={moviesPerPage}
				totalMovies={state.results.length}
				paginate={paginate}
			/>
		</div>
	);
};

export default App;
