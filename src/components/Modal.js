import ReactDom from 'react-dom';
import { Formik, Form, Field } from 'formik';
import OutsideClickHandler from 'react-outside-click-handler';
import { MdClose } from 'react-icons/md';
import { useState } from 'react';
import ReactStars from 'react-rating-stars-component';
import axios from 'axios';

const validate = (form) => {
	const date = new Date();
	const URL_expression =
		/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/;
	const parsedForm = {
		...form,
		year: parseInt(form.year),
	};
	if (parsedForm.year < 1000 || parsedForm.year > date.getFullYear()) {
		alert(`Year should be in range 1000...${date.getFullYear()}`);
		return null;
	}
	if (!parsedForm.image_url || parsedForm.image_url === 'null') {
		return { ...parsedForm, image_url: null };
	}
	if (!URL_expression.test(parsedForm.image_url)) {
		alert('Invalid URL');
		return null;
	}
	return parsedForm;
};

const Modal = ({ modalState, setModalState, children }) => {
	const [modalFormState, setModalFormState] = useState({
		...modalState,
		isOpen: false,
	});
	if (!modalState.isOpen) return null;
	const handleClickOutside = () => {
		setModalState((prev) => {
			return { ...prev, isOpen: false, movie: null };
		});
		setModalFormState((prev) => {
			return { ...prev, isOpen: false, movie: null };
		});
	};

	if (modalState.isOpen && !modalFormState.isOpen)
		return ReactDom.createPortal(
			<>
				<div className="overlay" />
				<OutsideClickHandler onOutsideClick={handleClickOutside}>
					<div className="modal">
						<img src={modalState.movie.image_url} alt="poster" />
						<div className="modalMain">
							<h2>{modalState.movie.title}</h2>
							<ReactStars
								count={5}
								onChange={() => console.log('changed')}
								value={modalState.movie.rating}
								size={50}
								activeColor="#ffd700"
							/>
							<p>Year: {modalState.movie.year}</p>
							<p>Genre: {modalState.movie.genre}</p>
							<p>Director: {modalState.movie.director}</p>
							<p>{modalState.movie.description}</p>
							<button
								className="btn btn-dark movie-edit"
								onClick={(prevState) =>
									setModalFormState({ ...prevState, isOpen: true })
								}
							>
								Edit movie
							</button>
						</div>
						<MdClose
							className="closeModal"
							onClick={(prev) =>
								setModalState({ ...prev, isOpen: false, movie: null })
							}
						></MdClose>
					</div>
				</OutsideClickHandler>
			</>,
			document.getElementById('portal')
		);
	else if (modalState.isOpen && modalFormState.isOpen)
		return ReactDom.createPortal(
			<>
				<div className="overlay" />
				<OutsideClickHandler onOutsideClick={handleClickOutside}>
					<div className="modal form">
						<Formik
							initialValues={{
								title: modalState.movie.title,
								year: modalState.movie.year,
								genre: modalState.movie.genre,
								director: modalState.movie.director,
								description: modalState.movie.description,
								image_url: modalState.movie.image_url,
							}}
							onSubmit={(values) => {
								const validated = validate(values);
								if (validated) {
									console.log(validated);
									axios
										.put(
											`https://my-website-314506-service-r2hahu3hnq-lm.a.run.app/movie/${modalState.movie.id}`,
											validated
										)
										.then(({ data }) => {
											alert('Successfully edited.');
										})
										.catch((error) => {
											alert('An unexpected problem has occurred.');
											console.log(error);
										});
								}
							}}
						>
							<Form>
								<Field name="title" as="textarea" />
								<Field name="year" as="textarea" />
								<Field name="director" as="textarea" />
								<Field name="genre" as="textarea" />
								<Field name="description" as="textarea" />
								<Field name="image_url" as="textarea" />
								<div className="buttons">
									<button
										className="btn btn-danger"
										type="button"
										onClick={(prev) =>
											setModalFormState({ ...prev, isOpen: false, movie: null })
										}
									>
										Deny
									</button>
									<button className="btn btn-success" type="submit">
										Accept
									</button>
								</div>
							</Form>
						</Formik>
						<MdClose
							className="closeModal"
							onClick={(prev) => {
								setModalState({ ...prev, isOpen: false, movie: null });
								setModalFormState({ ...prev, isOpen: false, movie: null });
							}}
						></MdClose>
					</div>
				</OutsideClickHandler>
			</>,
			document.getElementById('portal')
		);
};

export default Modal;
