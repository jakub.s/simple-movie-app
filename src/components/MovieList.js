import { useState } from 'react';
import Modal from './Modal';
import ReactStars from 'react-rating-stars-component';

const MovieList = (props) => {
	const [modalState, setModalState] = useState({
		isOpen: false,
		movie: null,
	});

	const ratingChanged = (newRating) => {
		console.log(newRating);
	};

	if (props.loading) {
		return <h2>Loading...</h2>;
	}
	return (
		<>
			{props.movies.map((movie, index) => (
				<div
					// onMouseEnter={() => console.log('Wszedł')}
					// onMouseLeave={() => console.log('Wyszedł')}
					key={movie.id}
					className="movie-box"
					movie={movie}
					onClick={() => {
						setModalState((prev) => {
							return { ...prev, isOpen: true, movie: movie };
						});
					}}
				>
					<img src={movie.image_url} alt="poster" />
					<div className="title">{movie.title}</div>
					<div className="stars" onClick={(e) => e.stopPropagation()}>
						<ReactStars
							count={5}
							onChange={ratingChanged}
							value={movie.rating}
							size={34}
							activeColor="#ffd700"
						/>
					</div>
				</div>
			))}
			<Modal modalState={modalState} setModalState={setModalState} />
		</>
	);
};

export default MovieList;
