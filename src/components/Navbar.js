const Navbar = (props) => {
	return (
		<section className="searchbox">
			<img
				src="https://image.flaticon.com/icons/png/512/633/633832.png"
				alt="movie-clapper-open"
			/>
			<input
				type="text"
				placeholder="Search for a movie..."
				className="form-control mr-sm-2 searchbox"
				onChange={props.handleInput}
				// onKeyPress={search}
			/>
		</section>
	);
};

export default Navbar;
